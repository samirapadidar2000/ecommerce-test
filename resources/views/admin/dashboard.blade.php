@extends('admin.layout.base')
@section('title', 'Dashboard')

@section('content')
    <div class="dashboard">
        <div class="row expanded">
            <h2>Dashboard</h2>
            {!! \App\classes\CSRFToken::_token() !!}
            <br />

            {!! \App\classes\Session::get('token') !!}
            {{ \App\classes\Redirect::to('/') }}
            {{$_SERVER['REQUEST_URI']}}

            <form action="/admin" method="post" enctype="multipart/form-data">
                <input name="product" value="testing">
                <input type="file" name="image">
                <input type="submit" name="submit" value="Go">
            </form>

             {{ print_r(\App\classes\Request::all()) }}
        </div>
    </div>
@endsection