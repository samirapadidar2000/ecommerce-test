@extends('admin.layout.base')
@section('content')
        <!DOCTYPE html>
<html>
<body>
<h3 style=" font-family: Arial, Helvetica, sans-serif; margin-left: 20%;">SELECTED POST</h3>
    <br><br><br><br>
    @if(count((array)$datas))
        @foreach($datas as $data)
            <div style="    margin-left: 20%;
            box-shadow: 0 0 10px -5px;
            padding: 59px;
            margin-right: 20%;
            border-radius: 10px;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: 600;">
                {{ $data['title_post']}}
                <br><br>
                {{$data['body_post']}}
            </div>
        @endforeach
@else
    <h3>You have not created any post</h3>
@endif
<div style="border-top: 1px solid #cdc4c4;margin-top: 60px;">
    <h3 style=" font-family: Arial, Helvetica, sans-serif; margin-left: 20%;">Comments</h3>
    @if(count((array)$comments))
        @foreach($comments as $comment)
            <div style="margin-left: 20%;
            box-shadow: 0 0 10px -5px;
            padding: 59px;
            margin-right: 20%;
            border-radius: 10px;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: 600;">
                {{ $comment->user_email}}
                <br><br>
                {{$comment->comments}}
            </div><br><br>
        @endforeach
    @else
        <h3>You have not created any post</h3>
    @endif
    <br><br><br><br><br>
    <h3 style="font-weight: 600; font-family: Arial, Helvetica, sans-serif; margin-left: 45%;">Post your comment</h3>
    <form action="/admin/products/categories/{{ $data['id'] }}/comment" method="post" style="margin-left: 25%; font-weight: 600; font-family: Arial, Helvetica, sans-serif;">
        <input type="email" name="email" placeholder="Enter your email" style="width: 732px; height: 31px; font-weight: 600; font-family: Arial, Helvetica, sans-serif;" value=""><br><br>
        <input type="text" name="comment" placeholder="Enter your comment" style="height: 30%;
            width: 732px;
            font-weight: 600;
            font-family: Arial, Helvetica, sans-serif;"><br><br>
        <input type="hidden" name="id_post_hid" value="{{ $data['id'] }}">
        <button type="submit" style="font-weight: 600; font-family: Arial, Helvetica, sans-serif;">SEND</button>
    </form>
</div>
@endsection
<br><br>
</body>