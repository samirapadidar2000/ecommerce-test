@extends('admin.layout.base')
@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
    </style>
</head>
   <body>
    <br><br><br><br>
    <form method="POST" action="/admin/products/categories/login">
        <input type="hidden" name="token" value="{{ \App\classes\CSRFToken::_token() }}">
        <button type="submit" >LOGIN</button>
    </form>
    <form action="/admin/products/categories" method="post" style="margin-left: 25%; font-weight: 600; font-family: Arial, Helvetica, sans-serif;">
            <input type="text" name="title" placeholder="Enter title your post" style="width: 732px; height: 31px; font-weight: 600; font-family: Arial, Helvetica, sans-serif;" value=""><br><br>
            <input type="text" name="body" placeholder="Enter body your post" style="height: 30%;
            width: 732px;
            font-weight: 600;
            font-family: Arial, Helvetica, sans-serif;"><br><br>
            <button type="submit" style="font-weight: 600; font-family: Arial, Helvetica, sans-serif;">SAVE</button>
        </form>
        <table style="border-collapse: collapse;
        width: 50%; margin-left: 25%;
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif;">
            <thead>
            <th style="border: 1px solid #dddddd; padding: 5px; font-weight: 600; font-family: Arial, Helvetica, sans-serif;">Title</th>
            <th style="border: 1px solid #dddddd; padding: 5px; font-weight: 600; font-family: Arial, Helvetica, sans-serif;">Edit</th>
            <th style="border: 1px solid #dddddd; padding: 5px; font-weight: 600; font-family: Arial, Helvetica, sans-serif;">Delete</th>
            <th style="border: 1px solid #dddddd; padding: 5px; font-weight: 600; font-family: Arial, Helvetica, sans-serif;">Read</th>
            </thead>
            <tbody>
            @if(count((array)$datas))
               @foreach($datas as $data)
                  <tr>
                      <td style="border: 1px solid #dddddd; padding: 5px; font-weight: 600; font-family: Arial, Helvetica, sans-serif;">{{ $data['title_post']}}</td>
                      <td style="border: 1px solid #dddddd; padding: 5px; font-weight: 600; font-family: Arial, Helvetica, sans-serif;">
                          <BUTTON ONCLICK="ShowAndHide{{$data['id']}}()" style="font-weight: 600; font-family: Arial, Helvetica, sans-serif;">Edit</BUTTON>
                          <DIV ID="SectionName{{$data['id']}}" STYLE=" display: none; border-radius: 10px; margin: 12px; box-shadow: 0 0 10px -3px; font-weight: 600; font-family: Arial, Helvetica, sans-serif;">
                              <form method="post" action="/admin/products/categories/{{ $data['id'] }}/edit" style="padding: 10px; width: 42px; font-weight: 600; font-family: Arial, Helvetica, sans-serif;">
                                  <input type="text" name="title" value="{{ $data['title_post'] }}" style="font-weight: 600; font-family: Arial, Helvetica, sans-serif;"><br><br>
                                  <input type="text" name="body" value="{{$data['body_post']}}" style="font-weight: 600; font-family: Arial, Helvetica, sans-serif;"><br><br>
                                  <input type="submit" class="button update-category" id="{{$data['id']}}" name="token" data-token="{{ \App\classes\CSRFToken::_token() }}" value="Update" style="font-weight: 600; font-family: Arial, Helvetica, sans-serif;">
                              </form>
                          </DIV>
                          <SCRIPT>
                              function ShowAndHide{{$data['id']}}() {
                                  var x = document.getElementById('SectionName{{$data['id']}}');
                                  if (x.style.display == 'none') {
                                      x.style.display = 'block';
                                  } else {
                                      x.style.display = 'none';
                                  }
                              }
                          </SCRIPT>
                      </td>
                      <td style="border: 1px solid #dddddd; padding: 5px; font-weight: 600; font-family: Arial, Helvetica, sans-serif;">
                          <form method="POST" action="/admin/products/categories/{{ $data['id'] }}/delete">
                              <input type="hidden" name="token" value="{{ \App\classes\CSRFToken::_token() }}" style="font-weight: 600; font-family: Arial, Helvetica, sans-serif;">
                              <button type="submit" style="font-weight: 600; font-family: Arial, Helvetica, sans-serif;">Delete</button>
                          </form>
                      </td>
                      <td style="border: 1px solid #dddddd; padding: 5px; font-family: Arial, Helvetica, sans-serif;">
                          <form method="get" action="/admin/products/categories/{{ $data['id'] }}/read">
                              <input type="hidden" name="token" value="{{ \App\classes\CSRFToken::_token() }}">
                              <button type="submit" style="font-weight: 600; font-family: Arial, Helvetica, sans-serif;">Read</button>
                          </form>
                      </td>
                  </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <h3>You have not created any post</h3>
    @endif
@endsection
   <br><br><br><br><br><br>
   </body>