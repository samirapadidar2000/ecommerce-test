<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Posts extends Model
{
    public $timestamps = true;
    public $fillable = ['title_post', 'body_post'];
}