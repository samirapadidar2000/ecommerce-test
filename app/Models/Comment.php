<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Comment extends Model
{

    public $timestamps = true;
    protected $fillable = ['user_email', 'comments', 'id_post'];
    protected $dates = ['deleted_at'];

}