<?php
$router = new AltoRouter;

$router->map('GET', '/', 'App\controllers\IndexController@show', 'home');

require_once __DIR__ . '/../controllers/admin/DashboardConteoller.php';

//for admin routes
$router->map('GET', '/admin', 'app\controllers\Admin\DashboardController@show', 'admin_dashboard');

$router->map('POST', '/admin', 'app\controllers\Admin\DashboardController@get', 'admin_form');

//Product management
require_once __DIR__ . '/../controllers/admin/ProductCategoryController.php';

$router->map('GET', '/admin/products/categories', 'app\controllers\Admin\ProductCategoryController@show', 'product_category');

$router->map('POST', '/admin/products/categories/[i:id]/delete', 'app\controllers\Admin\ProductCategoryController@delete', 'delete_product_category');

$router->map('POST', '/admin/products/categories', 'app\controllers\Admin\ProductCategoryController@store', 'create_product_category');

$router->map('POST', '/admin/products/categories/[i:id]/edit', 'app\controllers\Admin\ProductCategoryController@edit', 'edit_product_category');

$router->map('GET', '/admin/products/categories/[i:id]/read', 'app\controllers\Admin\ProductCategoryController@read', 'read_product_category');

$router->map('POST', '/admin/products/categories/[i:id]/comment', 'app\controllers\Admin\ProductCategoryController@addcomment', 'stor_product_category');

//$router->map('POST', '/admin/products/categories/login', 'app\controllers\ProductCategoryController@login', 'login_product_category');
$router->map('POST', '/admin/products/categories/login', 'app\controllers\Admin\ProductCategoryController@login', 'login_product_category');

//require_once __DIR__ .'/auth.php';
?>