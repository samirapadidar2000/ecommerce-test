<?php
namespace app\Controllers\Admin;
use App\classes\CSRFToken;
use App\classes\Redirect;
use App\classes\Request;
use App\classes\Session;
use App\classes\ValidateRequest;
use app\Controllers\BaseController;

use App\Models\Comments;
use App\Models\Posts;
use App\classes\Save;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Comment;

class ProductCategoryController extends BaseController
{
//show data from database in page***************************************************************************************
    public function show()
    {
        $datas = Posts::all();
        return view('admin/products/categories', compact('datas'));
    }

//save post from dsatabase**********************************************************************************************
    public function store()
    {
        if (Request::has('post')) {
            $request = Request::get('post');
                Posts::create([
                    'title_post' => $request->title,
                    'body_post' => $request->body
                ]);
                $datas = Posts::all();
                return view('admin/products/categories', compact('datas'));
            }
        return null;

        }

 //delete data from database********************************************************************************************
    public function delete($id)
    {
        if(Request::has('post')){
            $request = Request::get('post');

            if(CSRFToken::verifyCSRFToken($request->token)){
                Posts::destroy($id);
                Session::add('success', 'Category Deleted');
                Redirect::to('/admin/product/categories');
            }
           header("location:/admin/products/categories");
        }

        return null;
    }

//edit data from database***********************************************************************************************
    public function edit($id)
    {
        if(Request::has('post')){
            $request = Request::get('post');
                Posts::where('id', $id)->update(['title_post' => $request->title]);
                Posts::where('id', $id)->update(['body_post' => $request->body]);
            }
        header("location:/admin/products/categories");
    }

//show comments of post from database in page***************************************************************************
    public function comments($id){
        $comments = Capsule::table('comments')->where('id_post', '=', $id)->get();
        return $comments;
    }

//show all post with read button****************************************************************************************
    public function read($id)
    {
        $datas = Posts::where('id', '=', $id)->get();
        $comments = $this->comments($id);
        return view('admin/products/readpost', compact('datas','comments'));
    }
    public function addcomment()
    {
        if (Request::has('post')) {
            $request = Request::get('post');
            $user = User::where('email', $request->email)->first();
//                ->orWhere('email', $request->username)->first();

            if ($user) {
                Comment::create([
                    'user_email' => $request->email,
                    'comments' => $request->comment,
                    'id_post' => $request->id_post_hid
                ]);
                $datas = Comment::all();
                header('location:read');
            }else{
                header('location:read');
            }
        }
    }
    public function login()
    {

        if(Request::has('post')){
            $request = Request::get('post');
            if(CSRFToken::verifyCSRFToken($request->token)){
                $rules = [
                   'username' => ['required' => true],
                    'password' => ['required' => true],
                ];

                $validate = new ValidateRequest;
                $validate->abide($_POST, $rules);

                if($validate->hasError()){
                    $errors = $validate->getErrorMessages();
                    return view('admin/products/login', ['errors' => $errors]);
                }

                /**
                 * Check if user exist in db
                 */
                $user = User::where('username', $request->username)->first();
//                $user = User::where('username', $request->username)
//                    ->orWhere('email', $request->username);

                if($user){
                    if(!password_verify($request->password, $user->password)){
                        Session::add('error', 'Incorrect password');
                        return view('admin/products/login');
                    }else{
                        Session::add('SESSION_USER_ID', $user->id);
                        Session::add('SESSION_USER_NAME', $user->username);

                        if($user->role == 'admin'){
                            Redirect::to('/admin');
                        }else if($user->role == 'user' && Session::has('user_cart')){
                            Redirect::to('/cart');
                        }else{
                            Redirect::to('/');
                        }
                    }
                }else{
                    Session::add('error', 'User not found, please try again');
                    return view('admin/products/login');
                }
            }
        }
        return view('admin/products/login');
   }

}