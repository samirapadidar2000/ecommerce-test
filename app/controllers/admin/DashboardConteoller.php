<?php
namespace app\controllers\Admin;

use App\classes\Request;
use App\classes\Session;
use App\controllers\BaseController;

class DashboardController extends BaseController
{
    public function show()
    {
        Session::add('admin', 'You are welcom, admin user');
        Session::remove('admin');

        if(Session::has('admin')){
            $msg = Session::get('admin');
        }else{

            $msg = 'Not defined';
        }
        return view('admin/dashboard', ['admin' => $msg]);
    }
    public function get() {
        Request::refresh();
        $data = Request::old('post', 'products');
        var_dump($data);
    }
}