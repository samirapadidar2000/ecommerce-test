
<?php $__env->startSection('title', 'Dashboard'); ?>

<?php $__env->startSection('content'); ?>
    <div class="dashboard">
        <div class="row expanded">
            <h2>Dashboard</h2>
            <?php echo \App\classes\CSRFToken::_token(); ?>

            <br />

            <?php echo \App\classes\Session::get('token'); ?>

            <?php echo e(\App\classes\Redirect::to('/')); ?>

            <?php echo e($_SERVER['REQUEST_URI']); ?>


            <form action="/admin" method="post" enctype="multipart/form-data">
                <input name="product" value="testing">
                <input type="file" name="image">
                <input type="submit" name="submit" value="Go">
            </form>

             <?php echo e(print_r(\App\classes\Request::all())); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/ecommerce/resources/views/admin/dashboard.blade.php ENDPATH**/ ?>