<?php
//require_once __DIR__.'/../../../../app/controllers/admin/ProductCategoryController.php';

?>

<?php $__env->startSection('title', 'Product Category'); ?>

<?php $__env->startSection('content'); ?>
    <div class="Category">
        <div class="row expanded">
            <h2>Product Categories</h2>
            <br />
        </div>
        <?php if($message): ?>
            <p> <?php echo e($message); ?> </p>
        <?php endif; ?>
        <form action="" method="post">
            <input type="text" placeholder="Search by name">
            <input type="submit" value="search">
        </form>
        <form action="/admin/products/categories" method="post">
            <input type="text" name="name" placeholder="category name">
            <input type="hidden" name="token" value="<?php echo e(\App\classes\CSRFToken::_token()); ?>">
            <input type="submit" value="create">
        </form>



    <div>
        <?php if(count($categories)): ?>
            <table>
                <tbody>
                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($category->name); ?></td>
                            <td><?php echo e($category->slug); ?></td>
                            <td><?php echo e($category->created_at->toFormattedDateString()); ?></td>
                            <td>
                                <a href="#">edit</a>
                                <a href="#">delete</a>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        <?php else: ?>
            <h3>You have not created any category</h3>
        <?php endif; ?>
    </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/ecommerce/resources/views/admin/products/categories.blade.php ENDPATH**/ ?>